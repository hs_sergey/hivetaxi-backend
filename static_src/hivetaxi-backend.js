var API_HOST = "https://hivetaxi.ortoped.org.ru";

var resultCallbackFunction = null;

function hivetaxi_sendStatistics(operator_name, order_status, incoming_address, destination_address, incoming_phone, resultCallback) {
	resultCallbackFunction = resultCallback;
	var data = {
		operator_name: operator_name,
		order_status: order_status,
		incoming_address: incoming_address,
		destination_address: destination_address,
		incoming_phone: incoming_phone,
	};
	console.log("hivetaxi_sendStatistics");
	$.ajax({
		url: API_HOST + '/statistics_manager/add_statistics_record/',
		type: 'POST',
		data: JSON.stringify(data),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function(msg) {
			console.log(msg);
			if(resultCallbackFunction) {
				resultCallbackFunction(msg);
			}
		}
	});				
}
