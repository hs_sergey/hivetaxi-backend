# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.admin import ModelAdmin
from statistics_manager.models import StatisticsRecord

# Register your models here.
class StatisticsRecordAdmin(ModelAdmin):
	list_display = ('operator_name', 'incoming_phone', 'incoming_address', 'destination_address', 'order_status', 'created')
	change_list_template = "admin/statistics_records_change_list.html"
	search_fields = ['operator_name', 'incoming_phone', 'incoming_address', 'destination_address', 'order_status', 'created']
	
admin.site.register(StatisticsRecord, StatisticsRecordAdmin)	
	