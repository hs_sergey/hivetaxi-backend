# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.decorators.csrf import csrf_exempt
import json
import traceback
from django.http.response import JsonResponse, HttpResponseRedirect,\
	HttpResponse, Http404
from statistics_manager.models import StatisticsRecord
import urllib
import urllib2
import time
import datetime
from django.utils import timezone
from django.conf import settings
import xlsxwriter
from time import mktime

# Create your views here.
@csrf_exempt
def add_statistics_record(request):
	request_data = {}
	result = {}
	try:
		request_data = json.loads(request.body.decode("utf-8"))
		operator_name = request_data["operator_name"]
		order_status = request_data["order_status"]
		incoming_address = request_data["incoming_address"]
		destination_address = request_data["destination_address"]
		incoming_phone = request_data["incoming_phone"]
		statistics_record = StatisticsRecord(
			operator_name = operator_name,
			order_status = order_status,
			incoming_address = incoming_address,
			destination_address = destination_address,
			incoming_phone = incoming_phone,
		)
		statistics_record.save()
		result = {
			"status": "success",
			"result_code": "success",
			"message": "OK",
		}
	except Exception, e:
		traceback.print_exc()
		result = {
			"status": "error",
			"result_code": "bad_request",
			"message": str(e),
		}
	return JsonResponse(result)


def home_page(request):
	return HttpResponseRedirect("/admin")


@csrf_exempt
def send_to_telegram(request):	
	request_data = {}
	result = {}
	try:
		request_data = json.loads(request.body.decode("utf-8"))
		bot_token = request_data["bot_token"]
		chat_id = request_data["chat_id"]
		message = request_data["message"]
		URL = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s"
# 		encodedMessage = urllib.quote_plus(message)
		request = URL % (bot_token, chat_id, message)
		conn = urllib2.urlopen(request)
		content = conn.read()
		result = {
			"status": "success",
			"result_code": "success",
			"message": content,
		}
	except Exception, e:
		traceback.print_exc()
		result = {
			"status": "error",
			"result_code": "bad_request",
			"message": str(e),
		}
	return JsonResponse(result)
	
	
@csrf_exempt
def export_statistics(request):	
	if request.user.is_staff:
		statistics_date_from = request.POST.get("statistics_date_from")
		statistics_date_to = request.POST.get("statistics_date_to")
		date_from = datetime.datetime.fromtimestamp(time.mktime(time.strptime(statistics_date_from, "%d.%m.%Y")))
		date_from = datetime.datetime(date_from.year, date_from.month, date_from.day, 0, 0, 0, tzinfo=timezone.get_current_timezone())
		date_to = datetime.datetime.fromtimestamp(time.mktime(time.strptime(statistics_date_to, "%d.%m.%Y")))
		date_to = datetime.datetime(date_to.year, date_to.month, date_to.day, 0, 0, 0, tzinfo=timezone.get_current_timezone())
		records = StatisticsRecord.objects.filter(created__gte = date_from, created__lt = date_to).order_by("id")
		filename = settings.BASE_DIR + "/statistics.xlsx"
		workbook = xlsxwriter.Workbook(filename)
		worksheet = workbook.add_worksheet()
		bold = workbook.add_format({'bold': True})
		worksheet.set_column(0, 5, 40)
# 		worksheet.set_column(2, 3, 50)
		worksheet.write(0, 0, u"Оператор", bold)
		worksheet.write(0, 1, u"Телефон", bold)
		worksheet.write(0, 2, u"Адрес подачи", bold)
		worksheet.write(0, 3, u"Адрес назначения", bold)
		worksheet.write(0, 4, u"Статус заказа", bold)
		worksheet.write(0, 5, u"Создан", bold)
		row_index = 1
		for record in records:
			worksheet.write(row_index, 0, record.operator_name)
			worksheet.write(row_index, 1, record.incoming_phone)
			worksheet.write(row_index, 2, record.incoming_address)
			worksheet.write(row_index, 3, record.destination_address)
			worksheet.write(row_index, 4, record.order_status)
			worksheet.write(row_index, 5, timezone.localtime(record.created).strftime('%d.%m.%Y  %H:%M:%S'))
			row_index = row_index + 1
		workbook.close()
		fsock = open(filename, "r")
		response = HttpResponse(fsock, content_type = "application/excel")
		response['Content-Disposition'] = 'attachment; filename="statistics.xlsx"'
		return response
	else:
		raise Http404
	