# -*- coding: utf-8 -*-
from django.conf.urls import include, url

from . import views

urlpatterns = [
	url(r'^$', views.home_page, name='home_page'),
	url(r'^statistics_manager/add_statistics_record/', views.add_statistics_record, name='add_statistics_record'),
	url(r'^export_statistics/', views.export_statistics, name='export_statistics'),

	url(r'^send_to_telegram/', views.send_to_telegram, name='send_to_telegram'),
]
