Django==1.11.3
gunicorn==19.7.0
psycopg2==2.7.3.2
django-cors-headers==2.1.0
requests==2.18.4
XlsxWriter==1.0.2
